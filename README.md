# To Install #
Run following command on the ec2 instance

```
sudo vi /etc/rc.local
/usr/bin/curl -s https://bitbucket.org/enteksba/aws_cloud/raw/master/entek_ec2_startup.onstart | bash

exit 0;
```

Before restart, ensure that all necessary programs are installed by running the following;
```
curl -s https://bitbucket.org/enteksba/aws_cloud/raw/master/entek_ec2_startup.onstart | sudo bash
```

## Notes: ##
1. AWS CLI need to be installed.
1. AWS config file exists for root or EC2 should be attached to a role which allows changes to Route53 and Read EC2 Tags
1. AWS EC2 instance should have tag 'local_domain' and/or 'public_domain'

```
$> sudo apt-get install awscli
$> sudo aws configure

aws_access_key_id = AKIAJ6VFTAYATYOLP4ZA
aws_secret_access_key = <hidden>
region = ap-southeast-2
output = json
```

**Install script provides the following**
* Sets hostname from EC2 tag 'Name' on startup
* Creates/Updates public domain <EC2 TAG\[Name]>.<EC2 TAG\[public_domain]> with current public ip address
* Creates/Updates private domain <EC2 TAG\[Name]>.<EC2 TAG\[local_domain]> with current private ip address